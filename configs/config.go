package configs

import (
	"log"

	"github.com/spf13/viper"
)

type Config struct {
	AccountServiceUrl string `mapstructure:"ACCOUNT_SERVICE_URL"`
}

// LoadConfig reads configuration from file or environment variables.
func loadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName(".env")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}

func GetConfig() Config {
	path := "."
	config, err := loadConfig(path)
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	return config
}
