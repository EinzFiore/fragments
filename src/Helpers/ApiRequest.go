package helpers

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type ApiMeta struct {
	Version  int
	Message  string
	Errors   interface{}
	Metadata interface{}
	Results  interface{}
}

func HttpClient() *http.Client {
	client := &http.Client{Timeout: 10 * time.Second}
	return client
}

func RequestWithAuth(method string, url string, c *gin.Context) ([]byte, error) {
	endpoint := url
	client := HttpClient()

	token := c.GetHeader("Authorization")

	// hard payload, next will handle this
	payload := map[string]string{"foo": "baz"}
	jsonData, err := json.Marshal(payload)

	req, err := http.NewRequest(method, endpoint, bytes.NewBuffer(jsonData))
	if err != nil {
		log.Fatalf("Error Occurred. %+v", err)
	}

	req.Header.Add("Authorization", token)

	res, err := client.Do(req)
	if err != nil {
		log.Fatalf("Error sending request to API endpoint. %+v", err)
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Fatalf("Couldn't parse response body. %+v", err)
	}

	var result ApiMeta
	err = json.Unmarshal([]byte(string(body)), &result)
	if err != nil {
		return []byte(result.Message), err
	}

	if result.Errors != nil {
		return []byte(result.Message), errors.New("You are not authorized to access this resource")
	}

	// must to change results type interface to byte
	resData, err := json.Marshal(result.Results)
	if err != nil {
		return resData, err
	}

	// this helper only return results from response emis
	return resData, nil
}
